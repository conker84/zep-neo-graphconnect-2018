# zep-neo-graphconnect-2018

For your local setup, just change lines 16, 17, 27, 28 and 29 of neo4j-zeppelin.yml in order to provide a correct path.

Spin-up the stack

```
docker-compose -f .\neo4j-zeppelin.yml up
```